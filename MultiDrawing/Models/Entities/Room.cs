﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MultiuserDrawing.Models.Entities
{
    public class Room
    {
        public virtual int ID { get; set; }
        
        [Required(ErrorMessage = "Please enter room name")]        
        public virtual string Name { get; set; }

        public virtual string PictureUri { get; set; }

        public virtual List<ApplicationUser> Users { get; set; }
    }
}