﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MultiDrawing.Models;
using MultiuserDrawing.Models.Entities;
using System.IO;
using Newtonsoft.Json;
using CloudinaryDotNet.Actions;
using CloudinaryDotNet;
using System.Configuration;

namespace MultiDrawing.Controllers
{
    public class RoomsController : Controller
    {
        Dropbox.Api.DropboxAppClient client = new Dropbox.Api.DropboxAppClient("dgntwd9rgc5lyxm", "cfhnyal70pwxevr");

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Rooms
        public async Task<ActionResult> Index()
        {
            return View(await db.Rooms.ToListAsync());
        }

        public async Task<ActionResult> Rooms()
        {
            List<Room> rooms = await db.Rooms.ToListAsync();
            List<int> IDs = new List<int>(rooms.Select(i => i.ID).ToList());
            List<string> Names = new List<string>(rooms.Select(i => i.Name).ToList());
            return Json(new { IDs = IDs, Names = Names}, JsonRequestBehavior.AllowGet);            
        }

        public ActionResult Find(int? id)
        {
            if (id == null)
            {
                return Json(new { success = false, responseText = "Bad response" }, JsonRequestBehavior.AllowGet);
            }
            Room room = db.Rooms.ToList().Find(x => x.ID == id);
            if (room == null)
            {
                return Json(new { success = false, responseText = "Not found" }, JsonRequestBehavior.AllowGet);
            }
            List<string> names = room.Users.Select(u => u.UserName).ToList();
            return Json(new {names = names}, JsonRequestBehavior.AllowGet);
        }

        // GET: Rooms/Create
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name")] Room room)
        {
            if (ModelState.IsValid)
            {                
                db.Rooms.Add(room);
                await db.SaveChangesAsync();                
                return RedirectToAction("Index");
            }

            return View(room);
        }

        public JsonResult IsRoomExists(string RoomName)
        {            
            return Json(!db.Rooms.Any(x => x.Name == RoomName), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UploadCanvas(string canvasJson, string roomId)
        {            
            return Json(true);
        }

        [HttpPost]
        public async Task<ActionResult> Join(int? id)
        {
            if (id == null)
            {
                return Json(new { success = false, responseText = "Bad response" }, JsonRequestBehavior.AllowGet);
            }
            Room room = await db.Rooms.FindAsync(id);
            if (room == null)
            {
                return Json(new { success = false, responseText = "Not found" }, JsonRequestBehavior.AllowGet);
            }
            var user = db.Users.First(u => u.UserName == User.Identity.Name);
            room.Users.Add(user);
            await db.SaveChangesAsync();
            return Json(new { success = true, responseText = "User joined", pictureUri = room.PictureUri }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Leave(int? id)
        {
            if (id == null)
            {
                return Json(new { success = false, responseText = "Bad response" }, JsonRequestBehavior.AllowGet);
            }
            Room room = await db.Rooms.FindAsync(id);
            if (room == null)
            {
                return Json(new { success = false, responseText = "Not found" }, JsonRequestBehavior.AllowGet);
            }
            var user = db.Users.First(u => u.UserName == User.Identity.Name);
            room.Users.Remove(user);
            await db.SaveChangesAsync();
            return Json(new { success = true, responseText = "User left" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return Json(new { success = false, responseText = "Bad response" }, JsonRequestBehavior.AllowGet);
            }            
            Room room = await db.Rooms.FindAsync(Convert.ToInt32(id));
            if (room == null)
            {
                return Json(new { success = false, responseText = "Not found" }, JsonRequestBehavior.AllowGet);
            }
            if (room.Users.Count == 0 || (room.Users.Count == 1 && room.Users[0].UserName == User.Identity.Name))
            {
                db.Rooms.Remove(room);
                await db.SaveChangesAsync();
                return Json(new { success = true, responseText = "Deleted" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Room is not empty" }, JsonRequestBehavior.AllowGet);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
