﻿var container = document.getElementById("container");

Konva.angleDeg = false;

var width = container.clientWidth;
var height = container.clientHeight;

var drawing_enabled = false;

var resizing_enabled = false;

var chat = $.connection.drawHub;
$.connection.hub.start();

$(document).on('dragover', function (e) {      
    e.preventDefault(); return false;
});
$(document).on('drop', function (e) {
    e.preventDefault();
    e.originalEvent.dataTransfer.items[0].getAsString(function (url) {
        var groupid = uid()
        add_image_to_stage(url, stage.width() / 2 - 50, stage.height() / 2 - 50, groupid);        
        var data = {
            x: stage.width() / 2 - 50,
            y: stage.height() /2 - 50
        }
        chat.server.sendUrl(url, data, groupid);
    });    
});

$("#container").on('resize', function (e) {
    stage.width = this.width;
});

var stage = new Konva.Stage({
    container: 'container',
    width: width,
    height: height
});

function uid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}    

var layer = new Konva.Layer();
stage.add(layer);

var data = {
    x: 0,
    y: 0,
};

function add_image_to_stage(url, x, y, groupid)
{
    var img = new Image();
    img.src = url;
    $(img).load(function (e) {
        var coeff = img.naturalWidth / img.naturalHeight;        
        var image = new Konva.Image({
            image: img,
            x: 0,
            y: 0,      
            name: url,
            width: img.naturalWidth > 150 ? 150 : img.naturalWidth,
            height: img.naturalHeight > 150 / coeff ? 150 / coeff : img.naturalHeight,            
        })                     
        var img_group = new Konva.Group({
            x: x,
            y: y,
            name: groupid,
            draggable: true
        });        
        layer.add(img_group);
        img_group.add(image);
        addAnchor(img_group, 0, 0, 'topLeft');
        addAnchor(img_group, image.width(), 0, 'topRight');
        addAnchor(img_group, image.width(), image.height(), 'bottomRight');
        addAnchor(img_group, 0, image.height(), 'bottomLeft');        
        image.on('mouseover', function () {
            document.body.style.cursor = 'pointer';
        });
        image.on('mouseout', function () {
            document.body.style.cursor = 'default';
        });                   
        stage.draw();
    });   
}

var isPaint = false;
var lastPointerPosition;
var points = new Array();

var line = new Konva.Line({
    points: points,
    stroke: 'green',
    strokeWidth: 2,
    lineJoin: 'round',    
    lineCap: 'round',    
});

function JSONfromStage()
{
    return stage.toJSON();
}

stage.on('contentMousedown touchstart', function () {
    if (drawing_enabled)
    {
        isPaint = true;
        lastPointerPosition = stage.getPointerPosition();
        points.push(lastPointerPosition.x);
        points.push(lastPointerPosition.y);
        line.points = points;
        layer.add(line);
        chat.server.send(data, true);
    }    
});

function update(anchorName, group, anchorX, anchorY) {    
    var topLeft = group.get('.topLeft')[0];
    var topRight = group.get('.topRight')[0];
    var bottomRight = group.get('.bottomRight')[0];
    var bottomLeft = group.get('.bottomLeft')[0];    
    var image = group.get('Image')[0];
    // update anchor positions 
    switch (anchorName) {
        case 'topLeft':
            topRight.setY(anchorY);
            bottomLeft.setX(anchorX);
            break;
        case 'topRight':
            topLeft.setY(anchorY);
            bottomRight.setX(anchorX);
            break;
        case 'bottomRight':
            bottomLeft.setY(anchorY);
            topRight.setX(anchorX);
            break;
        case 'bottomLeft':
            bottomRight.setY(anchorY);
            topLeft.setX(anchorX);
            break;
    }
    image.position(topLeft.position()); 
    var width = topRight.getX() - topLeft.getX();
    var height = bottomLeft.getY() - topLeft.getY();
    if (width && height) {
        image.width(width);
        image.height(height);
    }
};
function addAnchor(group, x, y, name) {
    var stage = group.getStage();
    var layer = group.getLayer();
    var anchor = new Konva.Circle({
        x: x,
        y: y,
        stroke: '#666',
        fill: '#fff',
        strokeWidth: 2,
        radius: 10,
        opacity: 1,
        name: name,
        draggable: true,
        dragOnTop: false
    });
    anchor.on('dragmove', function (e) {
        if (!drawing_enabled)
        {            
            var anchorData = {
                name: this.getName(),
                group: group.name(),
                x: this.getX(),
                y: this.getY(),
            }            
            update(this.getName(), this.getParent(), this.getX(), this.getY());
            chat.server.sendAnchorData(group.name(), anchorData);
            layer.draw();
        }
    });
    anchor.on('mousedown touchstart', function () {
        if (!drawing_enabled)
        {
            group.setDraggable(false);
            this.moveToTop();            
        }
    });
    anchor.on('dragend', function () {
        if (!drawing_enabled)
        {
            group.setDraggable(true);
            layer.draw();
        }
    });    
    anchor.on('mouseover', function () {
        if (!drawing_enabled)
        {
            var layer = this.getLayer();
            document.body.style.cursor = 'pointer';
            this.setStrokeWidth(4);
            layer.draw();        
        }
    });
    anchor.on('mouseout', function () {
        if (!drawing_enabled)
        {
            var layer = this.getLayer();
            document.body.style.cursor = 'default';
            this.setStrokeWidth(2);
            layer.draw();                
        }        
    });
    group.add(anchor);
}

stage.on('contentMouseup touchend', function () {
    if (drawing_enabled)
    {
        isPaint = false;
        line.remove();
        line = new Konva.Line({
            points: points,
            stroke: 'green',
            strokeWidth: 2,
            lineJoin: 'round',
            lineCap: 'round',
        }); 
        layer.add(line);
        if (points.length < 6)
        {
            line.remove();
        }        
        stage.draw();        
        points = new Array();
        line = new Konva.Line({
            points: points,
            stroke: 'green',
            strokeWidth: 2,
            lineJoin: 'round',
            lineCap: 'round',
        }); 
    }    
});

stage.on('contentMousemove touchmove', function () {
    if (!isPaint) {       
        return;
    }        
    var pos = stage.getPointerPosition();
    var data = {
        x: pos.x,
        y: pos.y,
    };
    points.push(pos.x);
    points.push(pos.y);                              
    line.draw();    
    chat.server.send(data, false);
});

var pointsToDraw = new Array();
var lineToDraw = new Konva.Line({
    points: pointsToDraw,
    stroke: 'green',
    strokeWidth: 2,
    lineJoin: 'round',
    lineCap: 'round',
});

chat.client.addLine = function (data, newline) {
    if (newline)
    {
        stage.draw();
        lineToDraw.remove();
        lineToDraw = new Konva.Line({
            points: pointsToDraw,
            stroke: 'green',
            strokeWidth: 2,
            lineJoin: 'round',
            lineCap: 'round',
        });
        layer.add(lineToDraw);       
        pointsToDraw = new Array();
        lineToDraw = new Konva.Line({
            points: pointsToDraw,
            stroke: 'green',
            strokeWidth: 2,
            lineJoin: 'round',
            lineCap: 'round',
        });        
        layer.add(lineToDraw);    
    }
    else {
        pointsToDraw.push(data.x);
        pointsToDraw.push(data.y);        
        lineToDraw.draw();            
    }  
};

chat.client.addImg = function (url, data, groupid) {
    add_image_to_stage(url, data.x, data.y, groupid);
}

chat.client.dragImg = function (data, groupid) {
    group = stage.find('.' + groupid);    
    group.x(data.x);
    group.y(data.y);
    layer.draw();
}

chat.client.resizeImg = function (guid, data)
{
    var group = stage.find('.' + guid)[0];
    var anchor = group.find('.' + data.name);
    update(anchor[0].name(), group, data.x, data.y);
    layer.draw();
}

function change_dragability(value) {    
    var groups = stage.find('Group');        
    groups.each(function (group) {
        group.setDraggable(value);        
        layer.draw();
    })        
    var circles = stage.find('Circle');
    circles.each(function (circle) {
        circle.draggable(value);
        layer.draw();
    });
}

function choose_instrument(obj)
{    
    $(obj).parent().addClass('selected').siblings().removeClass('selected');    
    if (obj.id === "img_pointer")
    {
        drawing_enabled = false;
        change_dragability(true);
    }
    else if(obj.id === "img_brush"){
        drawing_enabled = true;
        change_dragability(false);
    }
}

chat.client.refreshUsers = function (id) {
    var numericId = id.replace(/[^0-9]/g, '');
    refresh_users(numericId)
};

chat.client.deleteRoom = function (roomId) {
    delete_room(roomId);
};

chat.client.updateRooms = function () {
    update_rooms();
};

var tempLayer = new Konva.Layer();
stage.add(tempLayer);
stage.on("dragstart touchstart", function (e) {
    if (!drawing_enabled)
    {
        layer.draw();        
    }
});
stage.on("dragend touchend", function (e) {
    if (!drawing_enabled) {
        layer.draw();
        tempLayer.draw();
    }
});

stage.on("dragmove", function (e) {
    if (!drawing_enabled) {
        var data = {
            x: e.target.x(),
            y: e.target.y()
        }
        chat.server.sendDragData(data, e.target.name());       
    }
});

stage.on("dragenter", function (e) {
    if (!drawing_enabled)
        layer.draw();
});
stage.on("dragleave", function (e) {
    if (!drawing_enabled)
        layer.draw();
});


