﻿var prevId = null;
var prevRoomId = null;

var logoffhref = document.getElementById("logoffLink");

$('#logoffLink').removeAttr("href");

$('#logoffLink').click(function () {
    leaveroom(prevRoomId, false);
    document.getElementById('logoutForm').submit()
});

window.onpagehide = function () {
    leaveroom(prevRoomId, false);
    //uploadCanvas(prevId);
}

window.onpageshow = function () {
    var chat = $.connection.drawHub;
    $.connection.hub.start().done(function () {
        chat.server.updateRooms();
    });
}

function showmenu(id) {
    if (prevId != id) {
        if (prevId != 0) {
            $('#' + prevId).nextAll().hide(400);
        }
        $('#' + id).nextAll().show(400);
        prevId = id;
    }
    else
    {
        $('#' + prevId).nextAll().hide(400);
        prevId = 0;
    }
};

function set_default_picture()
{
    //document.getElementById('canvasImg').setAttribute('src',"");
    //document.getElementById('invite_message').style.display = 'block';
    //document.getElementById('canvasImg').style.display = 'none';
};

function leaveroom(id, background) {
    var chat = $.connection.drawHub;
    $.connection.hub.start();
    if (id != null) {
        var numericId = id.replace(/[^0-9]/g, '');
        $.ajax({
            type: "POST",
            url: "/Rooms/Leave/" + numericId,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                if (response != null && response.success) {
                    $("#room" + numericId).nextAll().remove();
                    chat.server.leaveRoom("#room" + numericId);
                    if (!background)
                    {
                        //set_default_picture();
                    }                    
                    $("#room" + numericId).parent().append('<button onclick = "joinroom(this.id)" onload="hide()" id="room' + numericId +
                        'join" type="button" class="btn btn-default" '
                        + (background ? 'style="display:none"' : '')
                        +'>Join room</button>' +
                        '<button onclick = "deleteroom(this.id)" onload="hide()" id="room' + numericId +
                        'delete" type="button" class="btn btn-default"'
                        + (background ? 'style="display:none"' : '')
                        + '>Delete room</button>');
                    $('#users').html('');
                } else {
                    return false;
                }
            },
            error: function (response) {

            }
        })
    };
}

function joinroom(id) {
    var chat = $.connection.drawHub;
    $.connection.hub.start();
    if (prevRoomId != null && prevRoomId != id) {
        leaveroom(prevRoomId, true)
    }
    var numericId = id.replace(/[^0-9]/g, '');
    $.ajax({
        type: "POST",
        url: "/Rooms/Join/" + numericId,
        dataType: 'json',
        contentType: false, 
        processData: false,
        success: function (response) {
            if (response != null && response.success) {
                prevRoomId = id;
                $("#room" + numericId).nextAll().remove();
                chat.server.joinRoom("#room" + numericId);                
                $("#room" + numericId).parent().append('<button onclick = "leaveroom(this.id)" onload="hide()" id="room' + numericId +
                    'leave" type="button" class="btn btn-default">Leave room</button>');
                refresh_users(numericId);
            } else {
                alert(response.responseText);
            }
        },
        error: function (response) {

        }
    });
}

function refresh_users(roomId) {
    $.ajax({
        type: "GET",
        url: "/Rooms/Find/" + roomId,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            if (response != null) {
                $("#users").html('');
                response.names.forEach(addUser)
            }
        },
        error: function (response) {

        }
    });
}

function addUser(value, index, array) {
    $("#users").append('<tr style="border:none;"><td style="border: none;"><h5 style="margin:unset">' + value + '</h5></td></tr>');
}

function delete_room(roomId) {   
    $("#" + roomId).parent().parent().remove();
}

function update_rooms() {
    $.ajax({
        type: "GET",
        url: "/Rooms/Rooms",
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            if (response != null) {
                var numericId = response['IDs'][response['IDs'].length - 1];
                var name = response['Names'][response['Names'].length - 1];
                var room = document.getElementById("room" + numericId);                
                if (room == undefined)
                {
                    console.log(room);
                    $("#rooms").append('<tr style="width:inherit; border:none; padding:5px">'
                        + '<td id="td' + numericId + '" style="font-size:12px; border: none; padding: 10px">'
                        + '<h3 onclick="showmenu(this.id)" style="margin-top:unset" id="room' + numericId + '">' + name + '</h3>'
                        + '</td></tr>');
                    $("#room" + numericId).parent().append('<button onclick = "joinroom(this.id)" onload="hide()" id="room' + numericId +
                    'join" type="button" class="btn btn-default" style="display:none">Join room</button>' +
                    '<button onclick = "deleteroom(this.id)" onload="hide()" id="room' + numericId +
                    'delete" type="button" class="btn btn-default"style="display:none">Delete room</button>');
                }                
                
            }
        },
        error: function (response) {

        }
    });
}

function deleteroom(id) {
    var numericId = id.replace(/[^0-9]/g, '');
    $.ajax({
        type: "POST",
        url: "/Rooms/Delete/" + numericId,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            if (response != null && response.success) {
                var chat = $.connection.drawHub;
                $.connection.hub.start();                
                chat.server.deleteRoom("room" + numericId);
            } else {
                alert(response.responseText);
            }
        },
        error: function (response) {

        }

    });
}