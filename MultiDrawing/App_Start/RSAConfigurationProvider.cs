﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MultiDrawing
{
    public class RSAConfigurationProvider
    {
        public static void ProtectConfiguration()
        {
            Configuration config = null;
            List<ConfigurationSection> sections = new List<ConfigurationSection>();
            if (HttpContext.Current != null)
            {
                config =
                    System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            }
            else
            {
                config =
                    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            string provider =
                "RsaProtectedConfigurationProvider";
            sections.Add(config.ConnectionStrings);
            sections.Add(config.GetSection("system.net/mailSettings/smtp"));
            foreach (var section in sections)
            {
                if (section != null)
                {
                    if (!section.SectionInformation.IsProtected)
                    {
                        if (!section.ElementInformation.IsLocked)
                        {
                            section.SectionInformation.ProtectSection(provider);
                            section.SectionInformation.ForceSave = true;                            
                        }
                    }
                }
            }
            config.Save(ConfigurationSaveMode.Full);
        }
        
        public static void UnProtectConfiguration()
        {
            Configuration config = null;
            if (HttpContext.Current != null)
            {
                config =
                    System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
            }
            else
            {
                config =
                    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }
            List<ConfigurationSection> sections = new List<ConfigurationSection>();            
            sections.Add(config.ConnectionStrings);
            sections.Add(config.GetSection("system.net/mailSettings/smtp"));
            sections.Add(config.AppSettings);
            foreach (var section in sections)
            {
                if (section != null)
                {
                    if (section.SectionInformation.IsProtected)
                    {
                        if (!section.ElementInformation.IsLocked)
                        {
                            section.SectionInformation.UnprotectSection();

                            section.SectionInformation.ForceSave = true;
                            config.Save(ConfigurationSaveMode.Full);
                        }
                    }
                }
            }
        }
    }
}