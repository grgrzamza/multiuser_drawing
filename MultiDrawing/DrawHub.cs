﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MultiDrawing.Models;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace MultiDrawing
{
    public class DrawHub : Hub
    {
        public Task JoinRoom(string roomName)
        {
            Groups.Add(Context.ConnectionId, roomName);            
            return Clients.OthersInGroup(roomName).refreshUsers(roomName);                
        }

        public Task LeaveRoom(string roomName)
        {
            Groups.Remove(Context.ConnectionId, roomName);            
            return Clients.OthersInGroup(roomName).refreshUsers(roomName);
        }

        public Task DeleteRoom(string roomName)
        {
            return Clients.All.deleteRoom(roomName);
        }

        public Task UpdateRooms()
        {
            return Clients.AllExcept(Context.ConnectionId).updateRooms();
        }

        public void Send(DrawingData data, bool newline)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var User = db.Users.FirstOrDefault(u => u.UserName == Context.User.Identity.Name);
                    var roomName = "#room" + User.Rooms[0].ID;
                    Clients.OthersInGroup(roomName).addLine(data, newline);
                }
            }
            catch { };                                                          
        }

        public void SendAnchorData(string guid, AnchorData data)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var User = db.Users.FirstOrDefault(u => u.UserName == Context.User.Identity.Name);
                    var roomName = "#room" + User.Rooms[0].ID;
                    Clients.OthersInGroup(roomName).resizeImg(guid, data);
                }
            }
            catch { };
        }

        public void SendDragData(DrawingData data, string groupid)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var User = db.Users.FirstOrDefault(u => u.UserName == Context.User.Identity.Name);
                    var roomName = "#room" + User.Rooms[0].ID;
                    Clients.OthersInGroup(roomName).dragImg(data, groupid);
                }
            }
            catch { };
        }

        public void SendUrl(string url, DrawingData data, string groupid)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var User = db.Users.FirstOrDefault(u => u.UserName == Context.User.Identity.Name);
                    var roomName = "#room" + User.Rooms[0].ID;
                    Clients.OthersInGroup(roomName).addImg(url, data, groupid);
                }
            }
            catch { };
        }
    }
}